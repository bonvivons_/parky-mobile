package com.datategy.boujidaamine.contract;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.datategy.boujidaamine.ui.LocationText;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.subjects.PublishSubject;


public interface DisplayEstimationsContract {



        interface  View {
             IGeoPoint getMapCenter();
             String getSearchAdress();
            PublishSubject<LocationText> get_psTxt();
            List<String> getStores();
            ArrayAdapter<String> getAdapter();
            int getChosenHour();
            int getWday();
            MapView getMap();
            MapController getMapController();



        }

        interface Presenter {
            void doLoadEstimations();
            void link_debounce();
            void adressTOcoords(String query);
            void doSuggestionChosen(String query);
            void insert_report(Boolean occupied,String comment);

        }
    }


