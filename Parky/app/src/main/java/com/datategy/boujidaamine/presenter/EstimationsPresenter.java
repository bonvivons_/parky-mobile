package com.datategy.boujidaamine.presenter;

import android.util.Log;
import android.widget.ArrayAdapter;

import com.datategy.boujidaamine.contract.DisplayEstimationsContract;
import com.datategy.boujidaamine.ui.ColorUtils;
import com.datategy.boujidaamine.ui.LocationText;
import com.datategy.boujidaamine.ui.Street;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Polyline;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class EstimationsPresenter implements DisplayEstimationsContract.Presenter {

    public static final int cityRange = 30000;
    public static final int streetRange = 1200;
    private final CompositeDisposable compositeDisposable;


    private DisplayEstimationsContract.View view;
    List<String> storesPoint;
    public EstimationsPresenter(DisplayEstimationsContract.View view) {

        this.view=view;
        compositeDisposable = new CompositeDisposable(); //In Constructor


    }


    @Override
    public void doLoadEstimations() {
        IGeoPoint point=view.getMapCenter();
        Disposable disposable = Single.just(point)
                .map(p -> {
                    long starTime = System.nanoTime();
                    String map_center_lat = String.valueOf(point.getLatitude());
                    String map_center_lon = String.valueOf(point.getLongitude());
                    ArrayList<Polyline> lineArray = new ArrayList<Polyline>();
                    int z = 0;
                    int limit = 1;

                    ArrayList<Street> ptz = get_geo_estim(map_center_lon, map_center_lat, Integer.toString(streetRange), Integer.toString(cityRange), Integer.toString(view.getChosenHour()), view.getWday());
                    long staTime = System.nanoTime();
                    while (z < ptz.size()) {
                        lineArray.add(new Polyline());
                        lineArray.get(z).setWidth(5f);
                        float frac = Float.parseFloat(ptz.get(z).streetESTIM);
                        lineArray.get(z).setColor(ColorUtils.getColor(frac));
                        lineArray.get(z).setPoints(ptz.get(z).coordinates);
                        lineArray.get(z).setGeodesic(true);
                        z++;
                    }
                    long enTime = System.nanoTime();
                    long totaTime = enTime - staTime;
                    //Log.e("TIMESILLINPOLy", Long.toString(totaTime));
                    return lineArray;
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(polylines -> {
                    long staime = System.nanoTime();
                    for (Polyline line : polylines) {
                        view.getMap().getOverlayManager().add(line);
                    }

                }, throwable -> {
                    //Log.e("SOMETHING", throwable.getMessage());
                });
        compositeDisposable.add(disposable);


    }

    @Override
    public void link_debounce ( ) {
        Disposable disposable = view.get_psTxt()
                .debounce(100, TimeUnit.MILLISECONDS)

                .map(ps -> {
                     suggest(ps.getTxt(),ps.getLat(),ps.getLon());
                return "a";}).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(list -> {
                    int z = 1;
                    if (view.getStores().size() != 0) {
                        view.getAdapter().clear();
                        for (String obj : view.getStores()) {
                            view.getAdapter().add(obj);
                        }
                    }
                }, throwable -> {
                });
        compositeDisposable.add(disposable);

    }


    @Override
    public void adressTOcoords(String qu) {
        Disposable disposable = Single.just(qu)
                .map(quer -> {
                    String val1 = null;
                    String val2 = null;
                    String imageurl = "https://nominatim.openstreetmap.org/search?q=" + qu + "&format=geojson";
                    URL url = null;
                    String myout = null;
                    try {
                        url = new URL(imageurl);
                        URLConnection connection = url.openConnection();
                        String line;
                        StringBuilder builder = new StringBuilder();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                connection.getInputStream())); //problem is here
                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                        myout = builder.toString();
                        JSONObject jSONObject = new JSONObject(myout);
                        JSONObject tst = (jSONObject).getJSONArray("features").getJSONObject(0);
                        val1 = tst.getJSONObject("geometry").getJSONArray("coordinates").getString(0);
                        val2 = tst.getJSONObject("geometry").getJSONArray("coordinates").getString(1);
                        System.out.println(val1 + ' ' + val2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return (new GeoPoint(Double.parseDouble(val2), Double.parseDouble(val1)));
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(geop -> {
                    view.getMapController().animateTo(geop);
                   view.getMapController().scrollBy(0, 0);


                }, throwable -> {
                    //Log.e("SOMETHING", throwable.getMessage());
                });
        compositeDisposable.add(disposable);


    }

    @Override
    public void doSuggestionChosen(String qu) {
        Disposable disposable = Single.just(qu)
                .map(quer -> {
                    String val1 = null;
                    String val2 = null;
                    String imageurl = "https://nominatim.openstreetmap.org/search?q=" + qu + "&format=geojson";
                    URL url = null;
                    String myout = null;
                    try {
                        url = new URL(imageurl);
                        InputStream is = null;
                        URLConnection connection = url.openConnection();
                        String line;
                        StringBuilder builder = new StringBuilder();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(
                                connection.getInputStream())); //problem is here
                        while ((line = reader.readLine()) != null) {
                            builder.append(line);
                        }
                        myout = builder.toString();
                        JSONObject jSONObject = new JSONObject(myout);
                        JSONObject tst = (jSONObject).getJSONArray("features").getJSONObject(0);
                        val1 = tst.getJSONObject("geometry").getJSONArray("coordinates").getString(0);
                        val2 = tst.getJSONObject("geometry").getJSONArray("coordinates").getString(1);
                        System.out.println(val1 + ' ' + val2);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    return (new GeoPoint(Double.parseDouble(val2), Double.parseDouble(val1)));
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(geop -> {
                    view.getMapController().animateTo(geop);
                    view.getMapController().scrollBy(0, 0);


                }, throwable -> {
                    //Log.e("SOMETHING", throwable.getMessage());
                });
        compositeDisposable.add(disposable);
    }

    @Override
    public void insert_report(Boolean occupied, String comment) {

        Disposable disposable = Single.just("")
                .map(quer -> {
                   insert(occupied,comment);
                   return null;
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(geop -> {



                }, throwable -> {
                    Log.e("SOMETHING", throwable.getMessage());
                });
        compositeDisposable.add(disposable);

    }


    void suggest(String charseq, String lat, String lon) {



        if (!charseq.equals("") && charseq.length() >= 2) {
            String qu = charseq.replace(' ', '+');
            String imageurl = "https://services.gisgraphy.com/fulltext/suggest?format=json&suggest=true%20&style=long&lat=" + lat + "&lng=" + lon + "&radius=0&q=" + qu + "&apikey=12";
            URL url = null;
            String myout = null;
            try {
                url = new URL(imageurl);
                InputStream is = null;
                URLConnection connection = url.openConnection();
                String line;
                StringBuilder builder = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        connection.getInputStream())); //problem is here
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                myout = builder.toString();
                JSONObject jSONObject = new JSONObject(myout);
                JSONArray tst = (jSONObject).getJSONObject("response").getJSONArray("docs");
                int z = 0;
                int i = 0;
                while (i < tst.length() && z < 5) {
                    if (!view.getStores().contains(tst.getJSONObject(z).getString("name"))) {
                        view.getStores().add(tst.getJSONObject(z).getString("name"));
                        z++;

                    }
                    i++;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }




    }
    public ArrayList<Street> get_geo_estim(String lon, String lat, String streetRange, String cityRange, String hour, int day) {
        Connection c = null;
        int weekend;
        ArrayList<Street> geo_estim_tab = new ArrayList<Street>();
        if (day == 0 || day == 6) {
            weekend = 1;
        } else {
            weekend = 0;
        }
        ResultSet rs_dummy = null;
        ResultSet rs_streets_geom_preds = null;
        ////// GETTIN STREETS AND ESTIMATION FROM DB
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://54.36.54.157:5432/db_parky",
                            "root", "erengs");
            Statement st = c.createStatement();
            //if
            if (Double.parseDouble(lat) < 35.17 && Double.parseDouble(lat) > 10.83 && Double.parseDouble(lon) < 60.29 && Double.parseDouble(lon) > 31.99) {
                rs_dummy = st.executeQuery("SELECT get_closest_streets_khalij5(" + lat + ", " + lon + ", " + cityRange + ", " + streetRange + ", " + (day + 1) + ", " + hour + ")");
                rs_streets_geom_preds = st.executeQuery("SELECT * FROM public.result_table_khalij_temp");
            } else {
                rs_dummy = st.executeQuery("SELECT get_closest_streets2(" + lat + ", " + lon + ", " + cityRange + ", " + streetRange + ", " + weekend + ", " + hour + ")");
                rs_streets_geom_preds = st.executeQuery("SELECT * FROM public.result_table_temp");
            }
            while (rs_streets_geom_preds.next()) {
                String geometry = rs_streets_geom_preds.getString(1);
                geometry = geometry.replace("LINESTRING", "");
                geometry = geometry.replace("(", "");
                geometry = geometry.replace(")", "");
                String[] points = geometry.split(",", -1);
                ArrayList<GeoPoint> xyz = new ArrayList<GeoPoint>();
                for (String p : points) {
                    String[] splited = p.split("\\s+");

                    xyz.add(new GeoPoint(Double.parseDouble(splited[1]), Double.parseDouble(splited[0])));

                }
                Street strt = new Street(xyz, rs_streets_geom_preds.getString(2));
                geo_estim_tab.add(strt);
            }
            rs_dummy.close();
            rs_streets_geom_preds.close();
            st.close();
        } catch (Exception e) {
            e.printStackTrace();


        }////////////////
        return geo_estim_tab;
    }
    void insert(Boolean freeOc, String comment) {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://54.36.54.157:5432/db_parky",
                            "root", "erengs");
            Statement st = c.createStatement();
            st.executeQuery("INSERT INTO user_reports.user_reports_v01 (timestamp,center_lat,center_long,park_vacancy) VALUES ('2017-08-19 12:12:55-0400',42.124,2.2324,True);");
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
