package com.datategy.boujidaamine.ui;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.datategy.boujidaamine.contract.DisplayEstimationsContract;
import com.datategy.boujidaamine.presenter.EstimationsPresenter;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;

import java.util.List;

import io.reactivex.subjects.PublishSubject;

public class CustomDialogClass extends Dialog implements

        android.view.View.OnClickListener,DisplayEstimationsContract.View {
    @Override
    public IGeoPoint getMapCenter() {
        return null;
    }

    @Override
    public String getSearchAdress() {
        return null;
    }

    @Override
    public PublishSubject<LocationText> get_psTxt() {
        return null;
    }

    @Override
    public List<String> getStores() {
        return null;
    }

    @Override
    public ArrayAdapter<String> getAdapter() {
        return null;
    }

    @Override
    public int getChosenHour() {
        return 0;
    }

    @Override
    public int getWday() {
        return 0;
    }

    @Override
    public MapView getMap() {
        return null;
    }

    @Override
    public MapController getMapController() {
        return null;
    }

    private static Boolean occupButton = false;
    private static Boolean freeButton = false;
    private DisplayEstimationsContract.Presenter presenter;

    public Activity c;
    public Dialog d;
    TextView comm;
    public Button freePlace, ocuPlace,rep,cancl;

    public CustomDialogClass(Activity a) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.custom_dialog);
        presenter = new EstimationsPresenter(this) ;

        comm=findViewById(R.id.comment);
        freePlace = (Button) findViewById(R.id.free);
        ocuPlace = (Button) findViewById(R.id.ocup);
        rep=(Button) findViewById(R.id.btn_report);
        cancl=(Button) findViewById(R.id.btn_cancel);
        rep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (freeButton==false && occupButton==false){
                    Toast.makeText(getContext(),"Is the parking occupied already?",Toast.LENGTH_LONG).show();

                }
                else{
                    presenter.insert_report(freeButton,comm.getText().toString());
                    Toast.makeText(getContext(),"Thank you for your contribution !",Toast.LENGTH_LONG).show();
                    dismiss();
                }}









        });
        cancl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        freePlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                freeButton=true;
                if(occupButton==true){
                    ocuPlace.setBackground(getContext().getResources().getDrawable(R.drawable.occupiedparking_inactive));
                    occupButton=false;
                }
                freePlace.setBackground(getContext().getResources().getDrawable(R.drawable.freeparking_active256px));

            }
        });
        ocuPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                occupButton=true;
                if(freeButton==true){
                    freePlace.setBackground(getContext().getResources().getDrawable(R.drawable.freeparking_inactive256px));
                    freeButton=false;
                }
                ocuPlace.setBackground(getContext().getResources().getDrawable(R.drawable.occupied_parking_active256px));

            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ocup:
                c.finish();
                break;
            case R.id.free:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }



}