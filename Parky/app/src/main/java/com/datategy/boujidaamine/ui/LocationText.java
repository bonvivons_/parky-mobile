package com.datategy.boujidaamine.ui;
public  class LocationText {
    private String lat;
    private String lon;

    public LocationText(String txt, String lat, String lon) {
        this.lat = lat;
        this.lon = lon;
        this.txt = txt;
    }

    private String txt;

    public String getLat() {
        return lat;
    }

    public String getLon() {
        return lon;
    }

    public String getTxt() {
        return txt;
    }
}
