package com.datategy.boujidaamine.ui;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.datategy.boujidaamine.contract.DisplayEstimationsContract;
import com.datategy.boujidaamine.presenter.EstimationsPresenter;

import org.osmdroid.api.IGeoPoint;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.DelayedMapListener;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.text.DateFormatSymbols;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.PublishSubject;

import static android.support.constraint.Constraints.TAG;
import static android.widget.TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM;

public class MainActivity extends Activity implements DisplayEstimationsContract.View {
     public static ArrayAdapter<String> adapter;
    public static PublishSubject<LocationText> psTxt = PublishSubject.create();


    private DisplayEstimationsContract.Presenter presenter;
    public static List<String> stores = new ArrayList<String>();

    MapView map = null;
    public static  int chosen_hour = 9;
    public static int weeknd = 0;
    public static final int cityRange = 30000;
    public static final int streetRange = 1200;
    public static int sugfound = 0;
    public static double inituser_long = 39.826168;
    public static double inituser_lat = 21.422510;
    public static double initmap_lat = 21.422510;
    public static double initmap_long = 39.826168;
    public static LocationManager lm=null;
    MapController mMapController;
    SearchView city_adress;
    TextView datevi;
    Button datePickerButton, timePickerButton, reportButton;
    public static int searchin = 0;
    SearchView sv;
    Button currentLocButton;
    public static String mday;
    public static int mYear, mMonth, mDay, wDay, mMins, mHour;
    static double current_user_pos_long ,current_user_pos_lat;
    String name4 = null;
    Button eraseSearchQuery, rep;
    AutoCompleteTextView searchBar;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LocationListener mLocationListener = new LocationListener() {
            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onLocationChanged(final Location location) {
                //your code here
                current_user_pos_long=location.getLongitude();
                current_user_pos_lat=location.getLatitude();
            }

        };
        presenter = new EstimationsPresenter(this);

        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                5);
        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        searchBar = (AutoCompleteTextView) findViewById(R.id.autocomplete_country);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, stores);
        presenter.link_debounce();
        eraseSearchQuery = (Button) findViewById(R.id.dele);
        datePickerButton = (Button) findViewById(R.id.btn_date);
        currentLocButton = (Button) findViewById(R.id.mloc);
        CompositeDisposable notgeneric = new CompositeDisposable();
        timePickerButton = (Button) findViewById(R.id.timpik);
        datevi = (TextView) findViewById(R.id.datevie);
        reportButton = (Button) findViewById(R.id.report);
        initMap();
        setCurrentDateTime();
        updateLocation(mLocationListener);
        mMapController.setCenter(updateLocation(mLocationListener));
        stores.clear();
        searchBar.setAdapter(adapter);
        searchBar.setAutoSizeTextTypeWithDefaults(AUTO_SIZE_TEXT_TYPE_UNIFORM);
        datevi.setText(mDay + " " + (new DateFormatSymbols()).getMonths()[mMonth] + "," + " " + mday + " " + chosen_hour + ":" + mMins);

        searchBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                searchBar.setCursorVisible(true);
                sugfound = 0;
                searchin = 1;
                eraseSearchQuery.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.search_icon_128px));
            }
        });


        datePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(year, monthOfYear, dayOfMonth);
                                int dayOfweek = calendar.get(Calendar.DAY_OF_WEEK);

                                wDay = dayOfweek - 1;

                                if (wDay == 0) {
                                    mday = DayOfWeek.of(7).name().substring(0, 3);

                                } else {
                                    mday = DayOfWeek.of(wDay).name().substring(0, 3);
                                }

                                if (wDay == 0 || wDay == 6) {
                                    weeknd = 1;
                                } else {
                                    weeknd = 0;
                                }

                                mDay = dayOfMonth;
                                mYear = year;
                                mMonth = monthOfYear;

                                datevi.setText(mDay + " " + (new DateFormatSymbols()).getMonths()[mMonth] + "," + " " + mday + " " + chosen_hour + ":" + mMins);

                                //Toast.makeText(MainActivityFf.this, "Result: " + wDay, Toast.LENGTH_LONG).show();
                                mMapController.scrollBy(1, 0);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        //__
        timePickerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {
                        chosen_hour = hourOfDay;
                        mMins = minutes;
                        datevi.setText(mDay + " " + (new DateFormatSymbols()).getMonths()[mMonth] + "," + " " + mday + " " + chosen_hour + ":" + mMins);

                        //Toast.makeText(MainActivityFf.this, (chosen_hour) + ":00 h", Toast.LENGTH_LONG).show();
                        mMapController.scrollBy(0, 1);

                    }
                }, chosen_hour, mMins, false);
                timePickerDialog.show();

            }
        });


        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charseq, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //presenter.link_debounce();

                String charseq = editable.toString();
                psTxt.onNext(new LocationText(charseq,Double.toString(getMapCenter().getLatitude()),Double.toString(getMapCenter().getLongitude())));


            }
        });
        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onClickReport();
            }
        });


        map.setMapListener(new DelayedMapListener(new MapListener() {
            @Override
            public boolean onScroll(ScrollEvent event) {
                long startTime = System.nanoTime();
                Overlay obj = map.getOverlayManager().overlays().get(0); // remember first item
                map.getOverlayManager().overlays().clear(); // clear complete list
                map.getOverlayManager().overlays().add(obj);
                presenter.doLoadEstimations();
                // Log.e("TIMEmap", Long.toString(totalTime));
                return false;
            }

            @Override
            public boolean onZoom(ZoomEvent event) {
                //reloadMarker();
                // DECREASE LIMIT NUMBER
                return false;
            }
        }));



        eraseSearchQuery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchin == 1) {
                    //request
                    adapter.clear();

                    stores.clear();
                    adapter.notifyDataSetChanged();
                    searchBar.setCursorVisible(false);
                    String qu = searchBar.getText().toString().replace(' ', '+');

                    presenter.adressTOcoords(qu);
                    searchin = 0;
                    eraseSearchQuery.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.cross2));

                } else {
                    searchBar.setText("");
                    adapter.clear();
                    stores.clear();
                    sugfound = 0;
                }

            }
        });
        mMapController.scrollBy(0, 0);
        currentLocButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateLocation(mLocationListener);
                mMapController.animateTo(updateLocation(mLocationListener));

            }
        });


        searchBar.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


                searchin = 0;
                eraseSearchQuery.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.cross2));
                sugfound = 1;
                adapter.clear();
                stores.clear();

                searchBar.setCursorVisible(false);
                String item = adapterView.getItemAtPosition(i).toString();
                String qu = item.replace(' ', '+');
                ///
                presenter.doSuggestionChosen(qu);
            }
        });

    }


    public void onResume() {
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    public void onPause() {
        map.onPause();
        super.onPause();


    }
    @Override
    public PublishSubject<LocationText> get_psTxt() {
        return this.psTxt;
    }
    @Override
    public IGeoPoint getMapCenter() {
        return this.map.getMapCenter();
    }
    @Override
    public List<String> getStores() {
        return this.stores;
    }
    @Override
    public ArrayAdapter<String> getAdapter() {
        return  this.adapter;
    }
    @Override
    public String getSearchAdress() {
        return this.searchBar.getText().toString();
    }
    @Override
    public int getWday() {
        return this.wDay;
    }

    @Override
    public MapView getMap() {
        return this.map;
    }

    @Override
    public int getChosenHour() {
        return this.chosen_hour;
    }
    @Override
    public MapController getMapController() {
        return this.mMapController;
    }
    void initMap() {


        map = (MapView) findViewById(R.id.map);
        //final ITileSource tileSource = TileSourceFactory.OPEN_SEAMAP;
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.getTileProvider().getTileCache().getProtectedTileComputers().clear();
        map.getTileProvider().getTileCache().setAutoEnsureCapacity(false);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        mMapController = (MapController) map.getController();
        mMapController.setZoom(19);
        map.setBuiltInZoomControls(false);
        map.setMinZoomLevel(16.0);
        GpsMyLocationProvider prov = new GpsMyLocationProvider(this);

        MyLocationNewOverlay mLocOv = new MyLocationNewOverlay(prov, map);
        mLocOv.setDrawAccuracyEnabled(true);
        map.getOverlays().add(mLocOv);
    }
    GeoPoint updateLocation(LocationListener mLocationListener){
        Location location=null;

        if (
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                        ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            try {
                lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,1,
                        1, mLocationListener);
                location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                current_user_pos_long = location.getLongitude();
                current_user_pos_lat = location.getLatitude();


            } catch (Exception e) {
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER  ,1,
                        1, mLocationListener);
                location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                current_user_pos_long = location.getLongitude();
                current_user_pos_lat = location.getLatitude();

            }
        }
        GeoPoint usLoc=new GeoPoint(current_user_pos_lat,current_user_pos_long);
        return usLoc;

    }
    void setCurrentDateTime() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mMins = c.get(Calendar.MINUTE);
        mHour = c.get(Calendar.HOUR_OF_DAY);

        wDay = c.get(Calendar.DAY_OF_WEEK) - 1;

        chosen_hour = c.get(Calendar.HOUR_OF_DAY);
        if (wDay == 0 || wDay == 6) {
            weeknd = 1;
        } else {
            weeknd = 0;
        }
        if (wDay == 0) {
            mday = DayOfWeek.of(7).name().substring(0, 3);

        } else {
            mday = DayOfWeek.of(wDay).name().substring(0, 3);
        }
        //Log.e("WEEkndInit",weeknd+"");

    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 2:
                Log.d(TAG, "External storage2");
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.v(TAG, "Permission: " + permissions[0] + "was " + grantResults[0]);
                    //resume tasks needing this permission

                } else {

                }
                break;

            case 5: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                }
                return;
            }
        }
    }
    public void onClickReport() {

        CustomDialogClass cdd = new CustomDialogClass(MainActivity.this);
        cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cdd.show();
    }

}
