package com.datategy.boujidaamine.ui;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;

import org.json.JSONArray;
import org.json.JSONObject;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.DelayedMapListener;
import org.osmdroid.events.MapListener;
import org.osmdroid.events.ScrollEvent;
import org.osmdroid.events.ZoomEvent;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapController;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormatSymbols;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static android.support.constraint.Constraints.TAG;
import static android.widget.TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM;
//import android.graphics.Color;

public class MainActivityFf extends Activity {
    ArrayAdapter<String> adapter;

    MapView map = null;
    public static  int chosen_hour = 9;
    public static int weeknd = 0;
    public static final int cityRange = 30000;
    public static final int streetRange = 1200;
    public static int sugfound = 0;
    public static double user_pos_long = 39.826168;
    public static double user_pos_lat = 21.422510;
    public static double map_center_lat = 21.422510;
    public static double map_center_long = 39.826168;
    public static LocationManager lm=null;
    MapController mMapController;
    SearchView city_adress;
    TextView datevi;
    Button datePickerButton, timePickerButton, reportButton;
    public static int searchin = 0;
    SearchView sv;
    Button currentLocButton;
    public static String mday;
    public static int mYear, mMonth, mDay, wDay, mMins, mHour;
    String name4 = null;
    Button eraseSearchQuery, rep;
    AutoCompleteTextView searchBar;
    private static List<String> stores = new ArrayList<String>();
    ;

    /////////////
    /////////

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // REQUEST PERMISSION FOR LOCATION

        ///////////////////
        ////////THIS BLOC IS FOR GETTING USER LOCATION AND SETTING CENTER OF THE MAP ON USER LAT , LONG



        //////////////////////////
        //////////////////////


        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CustomDialogClass cdd = new CustomDialogClass(MainActivityFf.this);
                cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                cdd.show();
            }
        });




        //


        //adapter.setNotifyOnChange(true);


        ///////////////////////


    }


    //// RETURNS ARRAY CONTAINING STREET GEOMETRY AND ESTIMATION FOR EACH STREET IN A RANGE OF "streetrange" given hour , weekday , cityrange , lat and long

    /////// COVERTS RATIO TO HEX COLOR


    ///////Gets current date and set variable weeknd to 1 if it's a weekend

    /////////////////////////












    void insert(Boolean freeOc, String comment) {
        Connection c = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://54.36.54.157:5432/db_parky",
                            "root", "erengs");
            Statement st = c.createStatement();
            st.executeQuery("INSERT INTO user_reports.user_reports_v01 (timestamp,center_lat,center_long,park_vacancy) VALUES ('2017-08-19 12:12:55-0400',42.124,2.2324,True);");
            st.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

        public void onClickReport(View view) {

            CustomDialogClass cdd = new CustomDialogClass(MainActivityFf.this);
            cdd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            cdd.show();
        }


}







