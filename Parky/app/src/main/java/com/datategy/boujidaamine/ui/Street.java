package com.datategy.boujidaamine.ui;

import org.osmdroid.util.GeoPoint;

import java.util.List;

public class Street {
    public List<GeoPoint> coordinates;
    public String streetESTIM;
    public Street(List<org.osmdroid.util.GeoPoint> coordinatez ,String streetESTIM) {
        this.coordinates = coordinatez;
        this.streetESTIM=streetESTIM;
    }
}
